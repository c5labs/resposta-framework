<?php           defined('C5_EXECUTE') or die(_("Access Denied."));


 ?>
<div class="row">
<div class="footer">

<div class="fourcol">
<p class="author">Compiled by C5LABS</p>
 </div>
 <div class="eightcol last">
<p id="back-top"><a href="#top"><span>Back To Top</span></a></p>
 </div>
</div>
</div>
 
 
<!--The last lines of the template are defined by the core as shown below -->
</div>



<script  type="text/javascript" src="<?php    echo $this->getThemePath()?>/js/framework.js"></script>
<script  type="text/javascript" src="<?php    echo $this->getThemePath()?>/js/custom.js"></script>

<?php           Loader::element('footer_required'); ?>

</body>

</html>	